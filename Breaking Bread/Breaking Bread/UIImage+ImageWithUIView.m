//
//  UIImage+ImageWithUIView.m
//

#import "UIImage+ImageWithUIView.h"

@implementation UIImage (ImageWithUIView)
#pragma mark -
#pragma mark TakeScreenShot

+ (UIImage *)imageWithUIView:(UIView *)view
{
    UIImage* rendered;
    
    
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, true, 0);
    [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:false];
    rendered = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
  
  return rendered;
}

+ (UIImage *)imageWithUIView:(UIView *)view atScale:(CGFloat) scale
{
    UIImage* rendered;
    
    
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, true, scale);
    [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:false];
    rendered = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return rendered;
}

@end
