//
//  UIImage+ImageWithUIView.h
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface UIImage (ImageWithUIView)
+ (UIImage *)imageWithUIView:(UIView *)view;
+ (UIImage *)imageWithUIView:(UIView *)view atScale:(CGFloat) scale;
@end