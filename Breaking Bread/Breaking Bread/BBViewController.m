//
//  BBViewController.m
//  Breaking Bread
//
//  Created by Nathan Nesiba on 3/8/14.
//  Copyright (c) 2014 Myriad Mobile. All rights reserved.
//

#import "BBViewController.h"
#import "UIView+parallaxLayer.h"
#import "BBShrinkButton.h"
#import "UIImage+ImageWithUIView.h"
#import "BBLoginViewController.h"
#import "BreakingBreadServer.h"
#import "UIView+VisibilityAnimations.h"

@interface BBViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *backImageView;
@property (weak, nonatomic) IBOutlet UILabel *tagLabel;
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;
@property (weak, nonatomic) IBOutlet UIView *controlContainer;
@property (weak, nonatomic) IBOutlet BBShrinkButton *connectButton;
@property (weak, nonatomic) IBOutlet BBShrinkButton *registerButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingSpinner;
- (IBAction)logIn:(id)sender;
- (IBAction)register:(id)sender;

@property (strong) NSArray *backImages;
@property (strong) NSArray *lightness;
@property NSInteger imageIndex;

@end

@implementation BBViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    [self.backImageView addParallaxEffectWithDepth:1];
    
    [self.tagLabel setFont:[UIFont fontWithName:@"BreeSerif-Regular" size:23]];
    [self.tagLabel.layer setShadowColor:[UIColor whiteColor].CGColor];
    [self.tagLabel.layer setShadowOpacity:.5];
    [self.tagLabel.layer setShadowOffset:CGSizeMake(0, 0)];
    [self.tagLabel.layer setShadowRadius:2];
    
    [self.logoImage setTransform:CGAffineTransformMakeTranslation(0, 130)];
    
    [self.controlContainer setAlpha:0];
    [self.tagLabel setAlpha:0];
    [self.backImageView setAlpha:0];
    
    self.backImages = @[[UIImage imageNamed:@"meal3.jpg"],[UIImage imageNamed:@"guest.jpg"]];
    self.lightness = @[@(NO),@(YES)];
    
}


-(void) viewDidAppear:(BOOL)animated
{
    [self.backImageView setImage:[self.backImages objectAtIndex:self.imageIndex]];
    
    if(![BreakingBreadServer sharedServer].isLoggedIn)
    {
        [self performSelector:@selector(appear) withObject:nil afterDelay:1];
    }else{
        
        [self fakeLoginStep1];
    }

}

-(void) fakeLoginStep1
{
    [self.connectButton setHidden:YES withAnimationType:VisibilityAnimationTypePop duration:.3 completion:nil];
    
    [self performSelector:@selector(fakeLoginStep2) withObject:nil afterDelay:.05];
}

-(void) fakeLoginStep2
{
    [self.registerButton setHidden:YES withAnimationType:VisibilityAnimationTypePop duration:.3 completion:^{
        [self.loadingSpinner setAlpha:1];
        [self.loadingSpinner setHidden:YES];
        [self.loadingSpinner startAnimating];
        [self.loadingSpinner setHidden:NO withAnimationType:VisibilityAnimationTypePop duration:.3 completion:nil];
        
        [self performSelector:@selector(transitionToMain) withObject:nil afterDelay:2];
    }];
}

-(void) transitionToMain
{
    
    
    
    UIViewController *dashboard = [self.storyboard instantiateViewControllerWithIdentifier:@"dashboardNav"];
    
//    UIViewController *dashboard = [self.storyboard instantiateViewControllerWithIdentifier:@"profileNav"];
    
    UIView *dashView = dashboard.view;
    
    [self.view addSubview:dashView];
    
    [dashView setTransform:CGAffineTransformMakeTranslation(0, self.view.bounds.size.height)];
    [dashView setTransform:CGAffineTransformScale(dashView.transform, .9, .9)];
    
    CGAffineTransform translateUP = CGAffineTransformMakeTranslation(0, -self.view.bounds.size.height);
    
    [UIView animateWithDuration:.6 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.logoImage setTransform:translateUP];
        [self.tagLabel setTransform:translateUP];
        [self.controlContainer setTransform:translateUP];
        [dashView setTransform:CGAffineTransformTranslate(dashView.transform, 0, -self.view.bounds.size.height)];
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:.2 animations:^{
            [dashView setTransform:CGAffineTransformIdentity];
        } completion:^(BOOL finished) {
            [self presentViewController:dashboard animated:NO completion:nil];
        }];
        
    }];
}

-(void) appear
{
    
    [UIView animateWithDuration:.4 animations:^{

        [self.backImageView setAlpha:1];
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:.3 animations:^{
            [self.logoImage setTransform:CGAffineTransformIdentity];
            [self.tagLabel setAlpha:1];
            [self.controlContainer setAlpha:1];
        } completion:nil];
        
        
    }];
        
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)logIn:(id)sender
{
    [self showLoginPage];
}


-(void) showLoginPage
{
    UIImage *back = [UIImage imageWithUIView:[[UIApplication sharedApplication].windows objectAtIndex:0]];
    
    BBLoginViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"loginController"];
    
    [loginVC setBackImage:back];
    
    [self presentViewController:loginVC animated:NO completion:nil];
}

- (IBAction)register:(id)sender
{
    
}
@end
