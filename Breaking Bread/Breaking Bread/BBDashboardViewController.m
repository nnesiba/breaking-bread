//
//  BBDashboardViewController.m
//  Breaking Bread
//
//  Created by Nathan Nesiba on 3/8/14.
//  Copyright (c) 2014 Myriad Mobile. All rights reserved.
//

#import "BBDashboardViewController.h"

@interface BBDashboardViewController ()

@property (strong) NSArray *smartFeedContent;

@property (strong) NSArray *activeContent;

@end

@implementation BBDashboardViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"showMenu"] style:UIBarButtonItemStyleBordered target:self action:nil];

    [self.navigationItem setTitle:@"Dashboard"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableViewDelegate Methods-------------------------------------


-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

-(void) tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"DESELECT");
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   
}

#pragma mark UITableViewDatasourceMethods------------------------------------


-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    return self.activeContent.count;
    return 6;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;

        cell = [tableView dequeueReusableCellWithIdentifier:@"twitter"];
    
    if(indexPath.row == 1)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"instagram"];
    }
    
    
    if(indexPath.row == 2)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"recommendedHost"];
    }
    
    if(indexPath.row == 3)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"upcomingMeal"];
    }

    if(indexPath.row == 4)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"advertisement"];
    }
    
    if(indexPath.row == 5)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"twitter2"];
    }
    
    
    
    
    UIView* tag = [cell viewWithTag:5];

//    UIView * image = [cell viewWithTag:2];
//    
//    if(image)
//    {
//        [image.layer setCornerRadius:45];
//        [image.layer setBorderColor:[UIColor lightGrayColor].CGColor];
//        [image.layer setBorderWidth:1];
//    }
    
    if(tag)
    {
        [tag.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [tag.layer setBorderWidth:1];
    }

    
    return cell;
    
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        NSArray* array = [[NSBundle mainBundle] loadNibNamed:@"header" owner:nil options:nil];
        
        return [array objectAtIndex:0];
    }
    
    return nil;
}


@end
