//
//  BBShrinkButton.m
//  Breaking Bread
//
//  Created by Nathan Nesiba on 3/8/14.
//  Copyright (c) 2014 Myriad Mobile. All rights reserved.
//

#import "BBShrinkButton.h"

@implementation BBShrinkButton

-(void) setSelected:(BOOL)selected
{
    [super setSelected:selected];
    NSLog(@"SELECTED");
}

-(void) setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    
    if(highlighted)
    {
        [UIView animateWithDuration:.3 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            [self setTransform:CGAffineTransformMakeScale(.95, .95)];
        } completion:nil];
    }else{
        [UIView animateWithDuration:.3 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            [self setTransform:CGAffineTransformIdentity];
        } completion:nil];
    }
}

@end
