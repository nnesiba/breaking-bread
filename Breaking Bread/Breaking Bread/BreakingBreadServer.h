//
//  BreakingBreadServer.h
//  Breaking Bread
//
//  Created by Nathan Nesiba on 3/8/14.
//  Copyright (c) 2014 Myriad Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BreakingBreadServer : NSObject

@property (getter = isLoggedIn) BOOL loggedIn;

+(instancetype) sharedServer;


@end
