//
//  UIView+VisibilityAnimations.h
//  VirtualFarmManager
//
//  Created by Nathan Jangula on 8/15/13.
//  Copyright (c) 2013 Myriad Devices. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    VisibilityAnimationTypePop,
    VisibilityAnimationTypeFade
} VisibilityAnimationType;

typedef enum {
    TransitionAnimationTypeFlipUp,
    TransitionAnimationTypeFlipDown,
    TransitionAnimationTypeFlipLeft,
    TransitionAnimationTypeFlipRight
} TransitionAnimationType;

@interface UIView (VisibilityAnimations)

- (void)setHidden:(BOOL)hidden withAnimationType:(VisibilityAnimationType)animationType duration:(NSTimeInterval)duration completion:(void(^)(void))completion;

@end