//
//  UIView+VisibilityAnimations.m
//  VirtualFarmManager
//
//  Created by Nathan Jangula on 8/15/13.
//  Copyright (c) 2013 Myriad Devices. All rights reserved.
//

#import "UIView+VisibilityAnimations.h"

@implementation UIView (VisibilityAnimations)

const float kAnimationFraction = 1.0/3;
const float kFlipZoomFraction = 0.2;

- (void)setHidden:(BOOL)hidden withAnimationType:(VisibilityAnimationType)animationType duration:(NSTimeInterval)duration completion:(void (^)(void))completion {
    
    switch (animationType) {
        case VisibilityAnimationTypePop:
            
            if (hidden && !self.hidden) {
                [CATransaction begin];
                [CATransaction setCompletionBlock:^{
                    [self setHidden:hidden];
                    [self.layer removeAnimationForKey:@"poppingDown"];
                    
                    if(completion)
                    {
                        completion();
                    }
                }];
                
                CAKeyframeAnimation *popAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
                popAnimation.duration = duration;
                popAnimation.removedOnCompletion = false;
                popAnimation.fillMode = kCAFillModeForwards;
                popAnimation.additive = true;
                popAnimation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                                 [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                                 [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
                
                CATransform3D tinySize = CATransform3DMakeScale(.1, .1, 1);
                CATransform3D overSize = CATransform3DMakeScale(1.1, 1.1, 1);
        
                popAnimation.values = @[[NSValue valueWithCATransform3D:CATransform3DIdentity],[NSValue valueWithCATransform3D:overSize],[NSValue valueWithCATransform3D:tinySize]];
                
                [self.layer addAnimation:popAnimation forKey:@"poppingDown"];

                [CATransaction commit];
                
            } else if (!hidden && self.hidden) {

                [self setHidden:hidden];
                
                [CATransaction begin];
                [CATransaction setCompletionBlock:completion];
                CAKeyframeAnimation *popAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
                popAnimation.duration = duration;
                popAnimation.additive = true;
                popAnimation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                                 [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                                 [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
                
                CATransform3D tinySize = CATransform3DMakeScale(.1, .1, 1);
                
                CATransform3D overSize = CATransform3DMakeScale(1.1, 1.1, 1);
                
                popAnimation.values = @[[NSValue valueWithCATransform3D:tinySize],[NSValue valueWithCATransform3D:overSize],[NSValue valueWithCATransform3D:CATransform3DIdentity]];
                [self.layer addAnimation:popAnimation forKey:@"poppingUp"];
                [CATransaction commit];
                
                
            }
            
            break;
            
        case VisibilityAnimationTypeFade:
            
            if (!hidden) {
                [self setAlpha:0];
                [self setHidden:NO];
            }
            
            [UIView animateWithDuration:duration animations:^{
                
                [self setAlpha:(float)!hidden];
                
            } completion:^(BOOL finished) {
                
                [self setHidden:hidden];
                [self setAlpha:1];
                
                if (completion) completion();
                
            }];
            
            break;
            
    }
}



@end
