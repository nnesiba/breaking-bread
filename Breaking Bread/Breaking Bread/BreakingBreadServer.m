//
//  BreakingBreadServer.m
//  Breaking Bread
//
//  Created by Nathan Nesiba on 3/8/14.
//  Copyright (c) 2014 Myriad Mobile. All rights reserved.
//

#import "BreakingBreadServer.h"

@implementation BreakingBreadServer

static BreakingBreadServer *shared;


#pragma mark singleton

+(instancetype) sharedServer
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}




@end
