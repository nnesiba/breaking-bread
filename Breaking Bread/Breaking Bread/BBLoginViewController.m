//
//  BBLoginViewController.m
//  Breaking Bread
//
//  Created by Nathan Nesiba on 3/8/14.
//  Copyright (c) 2014 Myriad Mobile. All rights reserved.
//

#import "BBLoginViewController.h"
#import "BreakingBreadServer.h"

@interface BBLoginViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *backImageView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *backImageContainer;
- (IBAction)doLogin:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
- (IBAction)cancel:(id)sender;

@end

@implementation BBLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewWillAppear:(BOOL)animated
{
    [self.backImageView setImage:self.backImage];
    [self.contentView setTransform:CGAffineTransformMakeTranslation(0, self.view.bounds.size.height)];
}

-(void) viewDidAppear:(BOOL)animated
{
    [UIView animateWithDuration:.15 animations:^{
        [self.backImageContainer setTransform:CGAffineTransformMakeScale(.9, .9)];
    }];
    
    
    [UIView animateWithDuration:.4 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.contentView setTransform:CGAffineTransformIdentity];
    } completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.usernameField setDelegate:self];
    [self.passwordField setDelegate:self];
  
    
    UITapGestureRecognizer *tappityTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancel:)];
    
    [self.backImageView addGestureRecognizer:tappityTap];
    [self.backImageView setUserInteractionEnabled:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doLogin:(id)sender
{
    [self.view endEditing:YES];
    
    [[BreakingBreadServer sharedServer] setLoggedIn:YES];
    
    [self dismiss];
}


-(void) dismiss
{
    [UIView animateWithDuration:.4 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.contentView setTransform:CGAffineTransformMakeTranslation(0, self.view.bounds.size.height)];
        [self.backImageContainer setTransform:CGAffineTransformIdentity];
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:NO completion:nil];
    }];
}

#pragma mark UITextFieldDelegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:.3 animations:^{
        [self.contentView setTransform:CGAffineTransformMakeTranslation(0, -216)];
    }];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    
    [UIView animateWithDuration:.3 animations:^{
        [self.contentView setTransform:CGAffineTransformIdentity];
    }];
    
    return YES;
}

- (IBAction)cancel:(id)sender
{
    [self.view endEditing:YES];
    
    [self dismiss];
}
@end
