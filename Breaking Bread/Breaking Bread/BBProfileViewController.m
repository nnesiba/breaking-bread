//
//  BBProfileViewController.m
//  Breaking Bread
//
//  Created by Nathan Nesiba on 3/9/14.
//  Copyright (c) 2014 Myriad Mobile. All rights reserved.
//

#import "BBProfileViewController.h"

@interface BBProfileViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *backImageView;
@property (weak, nonatomic) IBOutlet UIImageView *profilePicView;

@end

@implementation BBProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.profilePicView.layer setCornerRadius:50];
    [self.profilePicView setClipsToBounds:YES];
}

-(void) viewDidAppear:(BOOL)animated
{
    [self performSelector:@selector(show) withObject:nil afterDelay:.6];
}

-(void) show
{
    [self.backImageView setHidden:NO];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
