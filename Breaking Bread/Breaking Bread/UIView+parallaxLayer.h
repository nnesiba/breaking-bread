//
//  UIView+parallaxLayer.h
//  parralaxTest
//
//  Created by Nathan Nesiba on 11/20/13.
//  Copyright (c) 2013 Nathan Nesiba. All rights reserved.
//

#import <UIKit/UIKit.h>

#define depthMultiplierX 10
#define depthMultiplierY 15

@interface UIView (parallaxLayer)

-(void) addParallaxEffectWithDepth:(NSInteger) depthLayer;

@end
