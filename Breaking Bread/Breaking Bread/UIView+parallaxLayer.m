//
//  UIView+parallaxLayer.m
//  parralaxTest
//
//  Created by Nathan Nesiba on 11/20/13.
//  Copyright (c) 2013 Nathan Nesiba. All rights reserved.
//

#import "UIView+parallaxLayer.h"

@implementation UIView (parallaxLayer)

-(void) addParallaxEffectWithDepth:(NSInteger) depthLayer
{
    UIInterpolatingMotionEffect *verticalMotionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    
    verticalMotionEffect.minimumRelativeValue = @((depthLayer*depthMultiplierY));
    
    verticalMotionEffect.maximumRelativeValue = @(-(depthLayer*depthMultiplierY));
    
    UIInterpolatingMotionEffect *horizontalMotionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    
    horizontalMotionEffect.minimumRelativeValue = @((depthLayer*depthMultiplierX));
    
    horizontalMotionEffect.maximumRelativeValue = @(-(depthLayer*depthMultiplierX));
    
    
    
    UIMotionEffectGroup *group = [UIMotionEffectGroup new];
    
    group.motionEffects = @[horizontalMotionEffect, verticalMotionEffect];
    
    [self addMotionEffect:group];
}

@end
