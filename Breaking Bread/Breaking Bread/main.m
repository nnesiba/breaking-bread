//
//  main.m
//  Breaking Bread
//
//  Created by Nathan Nesiba on 3/8/14.
//  Copyright (c) 2014 Myriad Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BBAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BBAppDelegate class]));
    }
}
